﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMove : MonoBehaviour
{
    public int maxDist = 1;
    private int isGoRight = -1;
    private float x;
    private float y;
    private bool lowCover;
    private bool stay;
    private float timer;
    private int lastSpeed;
    private int inBattle;
    private GameObject player;

    private float waitStopTimer;
    private bool waitStopFlag;
    private float minStopTimer;
    private int nextInBattle;
    private bool nextInBattleFlag;

    public int IIType;

    public void SetLowCover(bool lc)
    {
        lowCover = lc;
    }

    public int GetBattleStatus()
    {
        return inBattle;
    }

    public void SetBattleStatus(int bt)
    {
      /*  Debug.Log("+");
        Debug.Log(bt);
        if (bt == 2)
        {
            waitStopTimer = Time.time;
            waitStopFlag = true;
        }
        else
        {
            if (inBattle != 2)
            {
                nextInBattle = bt;
                minStopTimer = Time.time;
            }
            else
            {*/
                inBattle = bt;
          //  }
       // }
    }

    public void SetEnemyTarget(GameObject go)
    {
        player = go;
    }

    private bool m_Jump = false;


    private void Awake()
    {
        x = this.gameObject.transform.position.x;
        timer = Time.time;
        maxDist = 5;
      //  y = this.gameObject.transform.position.y;
    }

    private void Start()
    {
        x = this.gameObject.transform.position.x;
        inBattle = 0;
       // y = this.gameObject.transform.position.y;
    }


    private void Update()
    {
      //  if (!m_Jump)
     //   {
            // Read the jump input in Update so button presses aren't missed.
     //       m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            //    m_Jump = Input.GetKey(KeyCode.W);
     //   }
    }
    private bool CalcMaxRange()
    {
       // Debug.Log((x - this.gameObject.transform.position.x));// && (isGoRight == -1)));
        //De(((x - this.gameObject.transform.position.x) <= -maxDist) && (isGoRight == 1)));
        return ((((x - this.gameObject.transform.position.x) >= maxDist) && (isGoRight == -1)) || (((x - this.gameObject.transform.position.x) <= -maxDist) && (isGoRight == 1)));
    }


    private void FixedUpdate()
    {
     /*   if ((Time.time - waitStopTimer > 0.1) && waitStopFlag)
        {
            inBattle = 2;
            waitStopFlag = false;
            minStopTimer = Time.time;
            nextInBattleFlag = true;
            //Debug.Log(2);
        }
        if ((Time.time - minStopTimer > 1) && nextInBattleFlag)
        {
           // Debug.Log(7);
            inBattle = nextInBattle;
            nextInBattleFlag = false;
        }*/
        // Read the inputs.
        int speed = 0;
        // bool crouch = Input.GetKey(KeyCode.S) || lowCover;
        bool crouch = false;
        if (inBattle == 0)
        {
            if (IIType == 0)
            {
                if (Time.time - timer > 1)
                {
                    if (Time.time - timer < 5)
                    {
                        if (isGoRight == -1)
                        {
                            speed = -1;
                        }
                        if (isGoRight == 1)
                        {
                            speed = 1;
                        }
                        if (CalcMaxRange())
                        {
                            isGoRight = -isGoRight;
                            timer = Time.time;
                            lastSpeed = speed;
                        }
                    }
                    else
                    {
                        timer = Time.time;
                    }
                }
                else
                {
                    speed = 0;
                }
            }
            if (IIType == 1)
            {
                speed = 0;
            }
        }
        if (inBattle == 1)
        {
          //  Debug.Log(1);
            if(player.transform.position.x < this.gameObject.transform.position.x)
            {
                speed = -1;
                isGoRight = -1;
            }
            else
            {
                speed = 1;
                isGoRight = 1;
            }
        }
        if(inBattle == 2)
        {
            speed = 0;
          //  Debug.Log(2);
        }
        /*if (Input.GetKey(KeyCode.LeftControl))
        {
            this.gameObject.GetComponent<CharacterControl>().Move(0.51f * speed, crouch, m_Jump);
        }
        else
        {*/
        bool attack = false;
            this.gameObject.GetComponent<CharacterControl>().Move(0.3f * speed, crouch, attack, m_Jump);
        //}
        m_Jump = false;
    }
}