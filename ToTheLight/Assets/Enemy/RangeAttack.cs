﻿using UnityEngine;
using System.Collections;

public class RangeAttack : MonoBehaviour {

    public GameObject moveObject;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            //moveObject.GetComponent<EnemyMove>().SetEnemyTarget(other.gameObject);
            //Debug.Log(1);
            moveObject.GetComponent<EnemyMove>().SetBattleStatus(2);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {


    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            //moveObject.GetComponent<EnemyMove>().SetEnemyTarget(other.gameObject);
            moveObject.GetComponent<EnemyMove>().SetBattleStatus(1);
        }
    }
}
