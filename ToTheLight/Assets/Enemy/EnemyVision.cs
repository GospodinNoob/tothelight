﻿using UnityEngine;
using System.Collections;

public class EnemyVision : MonoBehaviour {

	// Use this for initialization

	void Start () {
	
	}

    public GameObject moveObject;
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            bool isFacingRight = moveObject.GetComponent<CharacterControl>().FacingRight();
            if (((other.gameObject.transform.position.x < moveObject.transform.position.x) && (!isFacingRight)) || ((other.gameObject.transform.position.x > moveObject.transform.position.x) && (isFacingRight))) 
            {
                moveObject.GetComponent<EnemyMove>().SetEnemyTarget(other.gameObject);
                if (!other.gameObject.GetComponent<Invisible>().GetInvisible())
                {
                    moveObject.GetComponent<EnemyMove>().SetBattleStatus(1);
                }
               // Debug.Log(other.gameObject.GetComponent<Invisible>().GetInvisible());
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            if (moveObject.GetComponent<EnemyMove>().GetBattleStatus() != 2)
            {
                bool isFacingRight = moveObject.GetComponent<CharacterControl>().FacingRight();
                if (((other.gameObject.transform.position.x < moveObject.transform.position.x) && (!isFacingRight)) || ((other.gameObject.transform.position.x > moveObject.transform.position.x) && (isFacingRight)))
                {
                    moveObject.GetComponent<EnemyMove>().SetEnemyTarget(other.gameObject);
                    if (!other.gameObject.GetComponent<Invisible>().GetInvisible())
                    {        
                        moveObject.GetComponent<EnemyMove>().SetBattleStatus(1);
                    }
                }
            }
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        
    }
}
