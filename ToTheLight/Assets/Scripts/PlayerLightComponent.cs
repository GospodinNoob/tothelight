﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerLightComponent : MonoBehaviour {

    private Dictionary<string, float> lightList = new Dictionary<string, float>();
    float transparence = 0;
    public GameObject darkLayer;
    public SpriteRenderer dark;
    private Color darkColor;
    private Color newColor;
    public Color cl;

    //  public Light lightSource = null;

    public bool GetEqualTF(string name)
    {
        return lightList.ContainsKey(name);
    }


    public void NewLight(string name, float value)
    {
        lightList.Add(name, value);
    }

    public void AddLight(string name, float value)
    {
        lightList.Remove(name);
        lightList.Add(name, value);
    }

    public void DeleteLight(string name)
    {
        lightList.Remove(name);
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame

    void Update()
    {
        transparence = 0;
        // int i = 0;
        foreach (var item in lightList)
        {
            transparence += item.Value;
            //    i++;
        }
        if (this.gameObject.tag != "Light")
        {
            // Debug.Log(dark.color.a);
            //Debug.Log(i);
            newColor.a = 0f;// transparence / 100;
            transparence = 1 - Math.Min(transparence, 1);
            //Debug.Log(transparence);
            this.GetComponent<Opacity>().SetLightTransparence(transparence); //dark.color = new Color(darkColor.r, darkColor.g, darkColor.b, transparence);
                                                                             //  darkColor = newColor;
        }
        else
        {
            if (this.gameObject.transform.root.gameObject.tag == "Player")
            {
                this.gameObject.GetComponent<LightCounter>().setPlusShowLight((int)transparence * 1000);
            }
            if (this.gameObject.transform.root.gameObject.tag == "Enemy")
            {
                this.gameObject.GetComponent<EnemyLight>().setPlusShowLight((int)transparence * 1000);
            }
        }
    }
}
