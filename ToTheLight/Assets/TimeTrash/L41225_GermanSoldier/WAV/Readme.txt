Spice up your World War II game (FPS or RTS) with this voice package containing 50 high quality voice samples of various german soldiers.

If you are not sure if this package is for you, please consider testing the FREE version of this package to preview some of the samples.

All samples were spoken by a German native speaker.

Sound format: WAV, 44 kHz, 16 BIT MONO


Suggestions and feedback (level41225@gmail.com)

Enjoy.