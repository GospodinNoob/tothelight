Thanks for checking out this free music asset on the Unity Asset Store.

This pack contains free versions of the third main track of the Horror Music Vol. I pack and its 
3 loops. The free track versions are in compressed mp3 (main track) and ogg (loops) formats, high 
quality 16 bit wav versions can be found on the full pack.

https://www.assetstore.unity3d.com/en/#!/content/38516

Don't forget to leave your rating on this free asset to help it reach more people.

Wish you all the best luck and success with your video game projects!

Best regards, T.I.D.N.Music