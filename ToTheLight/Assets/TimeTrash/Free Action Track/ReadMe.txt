Thanks for checking out this free music track on the Unity Asset Store.

This free music asset is a mp3 version of the third short track on the Action Music Vol. I pack.
A high quality wav version can be found on the full pack, as well as various loops based on the 
original track.

https://www.assetstore.unity3d.com/en/#!/content/37704

Don't forget to leave your rating on this free asset to help it reach more people.

Wish you all the best luck and success with your video game projects!

Best regards, T.I.D.N.Music