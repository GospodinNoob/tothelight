﻿using UnityEngine;
using System.Collections;

public class InvisibleZone : MonoBehaviour {

    int counter = 0;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Invisible")
        {
            counter++;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Invisible")
        {
            counter--;
        }
    }

    public bool GetInvisile()
    {
        if (counter > 0)
        {
            return true;
        }
        return false;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
