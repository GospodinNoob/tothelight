﻿using UnityEngine;
using System.Collections;

public class Invisible : MonoBehaviour {

    public GameObject invisibleZone1;
    public GameObject invisibleZone2;
    public GameObject invisibleZone3;

    // Use this for initialization
    public bool GetInvisible ()
    {
        bool invisible = true;
        invisible = invisible && invisibleZone1.GetComponent<InvisibleZone>().GetInvisile();
        invisible = invisible && invisibleZone2.GetComponent<InvisibleZone>().GetInvisile();
        invisible = invisible && invisibleZone3.GetComponent<InvisibleZone>().GetInvisile();

        invisible = invisible && (this.gameObject.layer == 21);

        return invisible;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
