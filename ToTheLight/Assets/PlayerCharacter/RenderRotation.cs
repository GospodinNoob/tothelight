﻿using UnityEngine;
using System.Collections;

public class RenderRotation : MonoBehaviour {

    private bool leftCover;
    private bool inCover;
    private bool isFacingRight;
    public GameObject renderObject;

    public void SetLookRight(bool tf)
    {
        isFacingRight = tf;
    }

    public void SetLeftCover(bool inC, bool tf)
    {
        inCover = inC;
        leftCover = tf;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (isFacingRight)
        {
            if (leftCover && inCover)
            {
                renderObject.transform.localRotation = new Quaternion(0, 180, 0, 1);
            }
            else
            {
                renderObject.transform.localRotation = new Quaternion(0, 0, 0, 1);
            }
        }
        else
        {
            if (leftCover && inCover)
            {
                renderObject.transform.localRotation = new Quaternion(0, 180, 0, 1);
            }
            else
            {
                if (inCover)
                {
                    renderObject.transform.localRotation = new Quaternion(0, 0, 0, 1);
                }
                else
                {
                    renderObject.transform.localRotation = new Quaternion(0, 180, 0, 1);
                }
            }
        }
	}
}
