﻿using UnityEngine;
using System.Collections;

public class SpriteLayerHelper : MonoBehaviour {

    public GameObject[] spriteList;
    public GameObject[] weaponList;

    private string nameLayer;

    public void SetLayerName(string s)
    {
        nameLayer = s;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    foreach (var item in spriteList)
        {
            item.GetComponent<SpriteRenderer>().sortingLayerName = nameLayer;
        }
        foreach (var item in weaponList)
        {
            if (this.gameObject.transform.root.gameObject.layer == 9)
            {
                item.layer = 12;
            }
            else
            {
                item.layer = 27;
            }
        }
    }
}
