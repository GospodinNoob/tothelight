﻿using UnityEngine;
using System.Collections;

public class DealDamage : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (((other.gameObject.transform.root.gameObject.layer == 10) && (this.gameObject.transform.root.gameObject.layer == 9)) || ((other.gameObject.transform.root.gameObject.layer == 26) && (this.gameObject.transform.root.gameObject.layer == 21)))
        {
            Damage dmg;
            dmg = new Damage();
            dmg.Ini();
            dmg.slashDamage++;
            other.gameObject.GetComponent<EnemyLight>().AddDamage(dmg);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {

    }

    void OnTriggerExit2D(Collider2D other)
    {

    }

    // Update is called once per frame
    void Update () {
	
	}
}
