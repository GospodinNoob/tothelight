﻿using UnityEngine;
using System.Collections;

public class OpenSpaceLayer : MonoBehaviour {

    int counter = 0;
    public GameObject layerObject;

    void OnTriggerEnter2D(Collider2D other)
    {
        counter++;
        layerObject.GetComponent<PlayerLayerScript>().SetSpaceZone(true);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        counter--;
        if (counter == 0)
        {
            layerObject.GetComponent<PlayerLayerScript>().SetSpaceZone(false);
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
