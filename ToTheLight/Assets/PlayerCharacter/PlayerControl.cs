﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerControl : MonoBehaviour
{
    private bool lowCover;

    public void SetLowCover(bool lc)
    {
        lowCover = lc;
    }

        private bool m_Jump;


        private void Awake()
        {
        }


        private void Update()
        {
            if (!m_Jump)
            {
            // Read the jump input in Update so button presses aren't missed.
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            //    m_Jump = Input.GetKey(KeyCode.W);
            }
        }

    float timer = -1;


    private void FixedUpdate()
    {
        // Read the inputs.
        int speed = 0;
        bool attack;
        bool crouch = Input.GetKey(KeyCode.S) || lowCover;
        if (Input.GetKey(KeyCode.A))
        {
            speed = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            speed = 1;
        }
        if (Input.GetKey(KeyCode.Mouse0) && (Time.time - timer > 0.6))
        {
            speed = 0;
            attack = true;
            timer = Time.time;
        }
        else
        {
            attack = false;
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            this.gameObject.GetComponent<CharacterControl>().Move(0.51f * speed, crouch, attack, m_Jump);
        }
        else
        {
            this.gameObject.GetComponent<CharacterControl>().Move(0.3f * speed, crouch, attack, m_Jump);
        }
        m_Jump = false;
     }
}