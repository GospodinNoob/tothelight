﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightCounter : MonoBehaviour {

	// Use this for initialization
    private int lightCounter;
    private int showLightCounter;
    private int showLight;
    private bool showLightFlag = false;
    private float maxLength = (float)32;
    private int maxLight = 1000;
    private GameObject sceneStarter;
    private float timer = Time.time;

    private int plusShowLight;

    public void setPlusShowLight(int sh)
    {
        plusShowLight = sh;
    }

    private Armour sumArmour;

    public void PlusLight(int light)
    {
        lightCounter += light;
    }

    private void MinusLight(int light)
    {
        lightCounter -= light;
        if (lightCounter <= 0)
        {
            Application.LoadLevel("MenuScene");
        }
    }

    public void setSumArmour(Armour arm)
    {
        sumArmour = arm;
    }

    public void AddDamage(Damage dmg)
    {
        dmg.ReduceDamage(sumArmour);
        int sum = dmg.SumDamage();
        if (sum > 0)
        {
            if (showLight + plusShowLight > 20)
            {
                MinusLight(sum);
            }
            else
            {
                Application.LoadLevel("MenuScene");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.GetComponent<LightComponent>().GetEqualTF(this.name))
        {
            other.GetComponent<LightComponent>().NewLight(this.name, 0);
        }
        else
        {
            other.GetComponent<LightComponent>().AddLight(this.name, 0);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        float dist = Vector2.Distance(this.transform.position, other.transform.position);
       // Debug.Log(lightCounter / maxLight * (maxLength - dist) / maxLength);
       if (showLight < 20)
        {
            other.GetComponent<LightComponent>().AddLight(this.name, 0);
        }
        else
        {
            other.GetComponent<LightComponent>().AddLight(this.name, (float)showLight / maxLight * (32 - dist) / maxLength);
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        other.GetComponent<LightComponent>().DeleteLight(this.name);
       // Debug.Log("Exit");
    }

    void Start () {
        lightCounter = 1000;
        timer = -Mathf.PI;
        maxLength = 32;
        foreach (GameObject start in GameObject.FindGameObjectsWithTag("SceneStarter"))
        {
            sceneStarter = start;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Q) && (timer + 1.57 <= Time.time))
        {
            showLightFlag = !showLightFlag;
            timer = Time.time;
           // Debug.Log(showLightFlag);
        }
        if (showLightFlag)
        {
            if (Time.time - timer <= Mathf.PI)
            {
                showLight = (int)(lightCounter * Mathf.Min((float)1, 1 - Mathf.Cos(Time.time - timer)));
            }
            else
            {
                showLight = lightCounter;
            }
        }
        else
        {
            if (Time.time - timer <= Mathf.PI)
            {
                showLight = (int)(lightCounter * Mathf.Max((float)0, Mathf.Cos(Time.time - timer)));
            }
            else
            {
                showLight = 0;
            }
        }
        if(showLight < 20)
        {
            showLight = 0;
            this.GetComponent<CircleCollider2D>().radius = 0;
            sceneStarter.GetComponent<SceneGenerator>().SetLight(1);
            //this.GetComponent<CircleCollider2D>().enabled = false;
        }
        else
        {
            //this.GetComponent<CircleCollider2D>().enabled = true;
            sceneStarter.GetComponent<SceneGenerator>().SetLight(1 - (float)showLight / (float)maxLight);
            this.GetComponent<CircleCollider2D>().radius = 32 * showLight / maxLight;
        }
	}
}
