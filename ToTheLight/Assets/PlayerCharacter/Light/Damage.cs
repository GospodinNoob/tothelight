﻿using UnityEngine;
using System.Collections;

public class Damage{

    public int piercingDamage;
    public int slashDamage;
    public int crashDamage;
    public int axeDamage;

    public void ReduceDamage(Armour arm)
    {
        this.piercingDamage -= arm.piercingArmour;
        this.slashDamage -= arm.slashArmour;
        this.crashDamage -= arm.crashArmour;
        this.axeDamage -= arm.axeArmour;
    }

    public int SumDamage()
    {
        int sum = 0;
        sum = piercingDamage + slashDamage + crashDamage + axeDamage;
        return sum;
    }

    public void Ini()
    {
        piercingDamage = 0;
        slashDamage = 0;
        crashDamage = 0;
        axeDamage = 0;
    }
}
