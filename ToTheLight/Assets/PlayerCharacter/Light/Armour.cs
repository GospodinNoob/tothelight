﻿using UnityEngine;
using System.Collections;

public class Armour{

    public int piercingArmour;
    public int slashArmour;
    public int axeArmour;
    public int crashArmour;

    public void Ini()
    {
        piercingArmour = 0;
        slashArmour = 0;
        axeArmour = 0;
        crashArmour = 0;
    }

    public void PlusArmour(Armour arm)
    {
        this.piercingArmour += arm.piercingArmour;
        this.slashArmour += arm.slashArmour;
        this.axeArmour += arm.axeArmour;
        this.crashArmour += arm.crashArmour;
    }
}
