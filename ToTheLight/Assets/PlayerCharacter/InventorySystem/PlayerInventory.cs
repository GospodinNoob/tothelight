﻿using UnityEngine;
using System.Collections;

public class PlayerInventory : MonoBehaviour {

    private Item[] quickMenu;
    private Item[,] inv;
    private GameObject chosen;
    public int n;
    public int m;
    private int pixSize = 28;
    private bool showInventory = false;
    private bool grabItem = false;
    private float timer;

    public GameObject throwItemBack;
    public GameObject throwItemMid;
    public GameObject lightObject;

    public GameObject throwPos;

    private Item weaponRight;
    private Item weaponLeft;
    private Item helmet;
    private Item body;
    private Item legs;
    private Item hands;
    private Item boots;

    public GameObject hitObject;

    public void PlusLight(int light)
    {
        lightObject.GetComponent<LightCounter>().PlusLight(light);
    }

    private void SwapItems(ref Item it1, ref Item it2)
    {
        Item help;
        help = it1.Copy();
        it1 = it2.Copy();
        it2 = help.Copy();

    }

    public void SetItem(Item it, int x, int y)
    {
        bool f = false;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (inv[i, j].id == 0)
                {
                    inv[i, j] = it.Copy();
                    chosen.GetComponent<ItemInventory>().ClearItem(x, y);
                    f = true;
                    break;
                }
            }
            if (f)
            {
                break;
            }
        }
    }

    private void SetItemToInv(Item it)
    {
        bool f = false;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (inv[i, j].id == 0)
                {
                    inv[i, j] = it.Copy();
                    weaponRight.Clear();
                    f = true;
                    break;
                }
            }
            if (f)
            {
                break;
            }
        }
    }

    public void ThrowItem(Item it, int x, int y)
    {
        GameObject go;
        if (gameObject.transform.parent.gameObject.layer == 9)
        {
            go = (GameObject)GameObject.Instantiate(throwItemMid, throwPos.gameObject.transform.position, throwPos.gameObject.transform.rotation);
            go.GetComponentInChildren<ItemInventory>().SetIniItem(it, x, y, this.gameObject);
        }
        else
        {
            go = (GameObject)GameObject.Instantiate(throwItemBack, throwPos.gameObject.transform.position, throwPos.gameObject.transform.rotation);
            go.GetComponentInChildren<ItemInventory>().SetIniItem(it, x, y, this.gameObject);
        }
    }

    public void SetChosen(GameObject go)
    {
        chosen = go;
    }

    public void ClearItem(int i, int j)
    {
        inv[i, j].Clear();
    }

    public void SetGrabItem(bool tf)
    {
        grabItem = tf;
    }

    Armour sumArmor;

    private void ResumArmour()
    {
        sumArmor.Ini();
        sumArmor.PlusArmour(hands.arm);
        sumArmor.PlusArmour(helmet.arm);
        sumArmor.PlusArmour(body.arm);
        sumArmor.PlusArmour(boots.arm);
        sumArmor.PlusArmour(legs.arm);
        hitObject.GetComponent<LightCounter>().setSumArmour(sumArmor);
    }

    private void LoadInventory()
    {
        for (int i = 0; i < 10; i++)
        {
            quickMenu[i] = new Item();
            quickMenu[i].id = 0;
        }
        inv = new Item[n, m];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                inv[i, j] = new Item();
                inv[i, j].Clear();
            }
        }
        weaponRight = new Item();
        weaponRight.Clear();
        weaponLeft = new Item();
        weaponLeft.Clear();
        helmet = new Item();
        helmet.Clear();
        boots = new Item();
        boots.Clear();
        body = new Item();
        body.Clear();
        hands = new Item();
        hands.Clear();
        legs = new Item();
        legs.Clear();
        sumArmor = new Armour();
        sumArmor.Ini();
    }

	// Use this for initialization
	void Start () {
        quickMenu = new Item[10];
        timer = 0;
        LoadInventory();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.I) && (timer + 0.3 <= Time.time) && (!grabItem))
        {
            showInventory = !showInventory;
            timer = Time.time;
        }
    }

    void OnGUI()
    {
        for(int i = 0; i < 10; i++)
        {
            if(GUI.Button(new Rect(Screen.width / 2 - pixSize * (5 - i), Screen.height - 3 * pixSize, pixSize, pixSize), ""))
            {
                
            }
        }
        if (showInventory || grabItem)
        {
           // Debug.Log(-1);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (2 - i), Screen.height / 4 + pixSize * (3 - j), pixSize, pixSize), "" + inv[i, j].id))
                    {
                        if ((Event.current.button == 0))
                        {
                            Debug.Log(0);
                            if (grabItem)
                            {
                                chosen.GetComponent<ItemInventory>().SetItem(inv[i, j], i, j);
                            }
                            else
                            {
                                //Debug.Log(inv[i, j].id);
                                if (inv[i, j].id == 1)
                                {
                                    SwapItems(ref inv[i, j], ref weaponRight);
                                  //  Debug.Log(1);
                                }
                                if (inv[i, j].id == 2)
                                {
                                    SwapItems(ref inv[i, j], ref weaponLeft);
                                }
                                if (inv[i, j].id == 3)
                                {
                                    SwapItems(ref inv[i, j], ref helmet);
                                }
                                if (inv[i, j].id == 4)
                                {
                                    SwapItems(ref inv[i, j], ref body);
                                }
                                if (inv[i, j].id == 5)
                                {
                                    SwapItems(ref inv[i, j], ref hands);
                                }
                                if (inv[i, j].id == 6)
                                {
                                    SwapItems(ref inv[i, j], ref legs);
                                }
                                if (inv[i, j].id == 7)
                                {
                                    SwapItems(ref inv[i, j], ref boots);
                                }
                            }
                        }
                        else
                        {
                            ThrowItem(inv[i, j], i, j);
                        }
                    }
                }
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (- 2 + n + 1), Screen.height / 4 + pixSize * (3 - m + 1), pixSize , pixSize ), "" + weaponRight.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(weaponRight);
                }
                else
                {
                    ThrowItem(weaponRight, 0, 0);
                }
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 1), Screen.height / 4 + pixSize * (3 - m + 2), pixSize , pixSize ), "" + weaponLeft.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(weaponLeft);
                }
                else
                {
                    ThrowItem(weaponLeft, 0, 0);
                }
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 2), Screen.height / 4 + pixSize * (3 - m + 1), pixSize , pixSize ), "" + helmet.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(helmet);
                }
                else
                {
                    ThrowItem(helmet, 0, 0);
                }
                ResumArmour();
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 2), Screen.height / 4 + pixSize * (3 - m + 2), pixSize , pixSize ), "" + body.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(body);
                }
                else
                {
                    ThrowItem(body, 0, 0);
                }
                ResumArmour();
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 2), Screen.height / 4 + pixSize * (3 - m + 3), pixSize , pixSize ), "" + legs.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(legs);
                }
                else
                {
                    ThrowItem(legs, 0, 0);
                }
                ResumArmour();
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 2), Screen.height / 4 + pixSize * (3 - m + 4), pixSize , pixSize ), "" + hands.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(hands);
                }
                else
                {
                    ThrowItem(hands, 0, 0);
                }
                ResumArmour();
            }
            if (GUI.Button(new Rect(Screen.width / 4 - pixSize * (-2 + n + 2), Screen.height / 4 + pixSize * (3 - m + 5), pixSize , pixSize ), "" + boots.id))
            {
                if ((Event.current.button == 0))
                {
                    SetItemToInv(boots);
                }
                else
                {
                    ThrowItem(boots, 0, 0);
                }
                ResumArmour();
            }
        }
    }
}
