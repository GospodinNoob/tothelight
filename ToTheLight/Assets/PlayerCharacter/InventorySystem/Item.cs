﻿using UnityEngine;
using System.Collections;

/*id
 1 - оружие
 2 - оружие в левую руку
 3 - шлем
 4 - торс
 5 - перчатки
 6 - ноги
 7 - ботинки 
 */

public class Item
{
    public int id = 0;
    public Damage dmg;
    public Armour arm;
    
    public void Generate()
    {
        this.dmg = new Damage();
        this.arm = new Armour();
        this.id = Random.Range(0, 8);
    }

    public void Clear()
    {
        this.id = 0;
        this.dmg = new Damage();
        this.arm = new Armour();
        this.dmg.Ini();
        this.arm.Ini();
    }

    public Item Copy()
    {
        Item copyItem = new Item();
        copyItem.id = this.id;
        return copyItem;
    }
}
